﻿using BrainerHubTaskWebApi.DomainLayer.Models;
using BrainerHubTaskWebApi.Helpers;
using BrainerHubTaskWebApi.RepositoryLayer.Interface;
using BrainerHubTaskWebApi.ServiceLayer.Interface;
using BrainerHubTaskWebApi.Specification;

namespace BrainerHubTaskWebApi.ServiceLayer.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task AddProduct(Products products)
        {
            try
            {
                await _productRepository.AddProduct(products);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        public async Task<Products> GetById(int id)
        {
            try
            {
                return await _productRepository.GetById(id);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        public async Task<PagedList<Products>> GetProductList(ProductFilter filter)
        {
            try
            {

                var specifications = new List<ISpecification<Products>>
                {

                new ProductNameSpecification(filter.ProductName),
                new NotDeletedSpecification(),
                };

                var predicate = specifications
                    .Where(specification => specification.Criteria != null)
                    .Select(specification => specification.Criteria)
                    .Aggregate((currentPredicate, nextPredicate) => currentPredicate.And(nextPredicate));

                IEnumerable<Products> filterProducts = await _productRepository.GetAllProducts(predicate);

                var data = PagedList<Products>
                           .ToPagedList(
                               filterProducts.OrderBy(o => o.ProductId),
                               filter.PageNumber,
                               filter.PageSize);
                return data;
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        public Task<bool> ProductIsExists(string productName)
        {
            return _productRepository.ProductIsExists(productName);
        }

        public async Task<bool> RemoveProduct(int id)
        {
            try
            {
                return await _productRepository.RemoveProduct(id);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        public async Task UpdateProduct(Products products)
        {
            try
            {
                await _productRepository.UpdateProduct(products);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }
    }
}