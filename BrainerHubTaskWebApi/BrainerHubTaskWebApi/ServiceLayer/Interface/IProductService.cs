﻿using BrainerHubTaskWebApi.DomainLayer.Models;
using BrainerHubTaskWebApi.Helpers;

namespace BrainerHubTaskWebApi.ServiceLayer.Interface
{
    public interface IProductService
    {
        Task<PagedList<Products>> GetProductList(ProductFilter filter);

        Task<Products> GetById(int id);

        Task AddProduct(Products products);

        Task<bool> RemoveProduct(int id);

        Task UpdateProduct(Products products);

        Task<bool> ProductIsExists(string productName);
    }
}
