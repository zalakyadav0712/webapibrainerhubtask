﻿using BrainerHubTaskWebApi.DomainLayer.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace BrainerHubTaskWebApi.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<Users>
    {
        public void Configure(EntityTypeBuilder<Users> builder)
        {
            builder.ToTable("Users"); 
            builder.HasKey(x => x.Id); 
            builder.Property(x => x.Id).HasColumnName("UserId"); 
            builder.Property(x => x.Email).HasMaxLength(255).IsRequired(); 
            builder.Property(x => x.Password).HasMaxLength(255).IsRequired(); 
            builder.Property(x => x.FirstName).HasMaxLength(100).IsRequired();
            builder.Property(x => x.LastName).HasMaxLength(100).IsRequired(); 
        }

    }
}
