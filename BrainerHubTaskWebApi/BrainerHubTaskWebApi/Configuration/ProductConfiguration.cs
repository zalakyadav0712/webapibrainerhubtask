﻿using BrainerHubTaskWebApi.DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BrainerHubTaskWebApi.Configuration
{
    public class ProductConfiguration : IEntityTypeConfiguration<Products>
    {
        public void Configure(EntityTypeBuilder<Products> builder)
        {
            builder.ToTable("Products", "brn");
            builder.HasKey(x => x.ProductId);
            builder.Property(x => x.ProductName).HasMaxLength(100).IsRequired(false);
            builder.Property(x => x.Description).HasMaxLength(500).IsRequired(false);
            builder.Property(x => x.Price).IsRequired(false);
            builder.Property(x => x.Quentity).IsRequired(false);
            builder.Property(x => x.IsDeleted).HasDefaultValue(false).IsRequired(false);
        }
    }
}
