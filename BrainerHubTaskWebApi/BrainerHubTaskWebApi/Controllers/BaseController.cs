﻿using BrainerHubTaskWebApi.DomainLayer.Models;
using Microsoft.AspNetCore.Mvc;

namespace BrainerHubTaskWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        [HttpGet]
        [Route("result-notfound")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public ObjectResult ResultNotFound()
        {
            return StatusCode(StatusCodes.Status404NotFound, new CommonResponse
            {
                StatusCode = StatusCodes.Status404NotFound,
                IsValid = false,
                Message = "No Data Found",
                //ResponseData = responseData,
            });
        }

        [HttpGet]
        [Route("result-badrequest")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public ObjectResult ResultBadRequest()
        {
            return StatusCode(StatusCodes.Status400BadRequest, new CommonResponse
            {
                StatusCode = StatusCodes.Status400BadRequest,
                IsValid = false,
                Message = "Failed",
            });
        }

        [HttpGet]
        [Route("result-badrequest")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public ObjectResult ResultExixstBadRequest(string msg)
        {
            return StatusCode(StatusCodes.Status400BadRequest, new CommonResponse
            {
                StatusCode = StatusCodes.Status400BadRequest,
                IsValid = false,
                Message = msg,
            });
        }

        [HttpGet]
        [Route("result-badrequest-validation")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public ObjectResult ResultBadRequestValidation(Object ResponseData)
        {
            return StatusCode(StatusCodes.Status400BadRequest, new CommonResponse
            {
                StatusCode = StatusCodes.Status400BadRequest,
                IsValid = false,
                Message = "Id is required",
                ResponseData = null,
            });
        }

        [HttpGet]
        [Route("result-ok")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public ObjectResult ResultOk(object responseData)
        {
            return StatusCode(StatusCodes.Status200OK, new CommonResponse
            {
                StatusCode = StatusCodes.Status200OK,
                IsValid = true,
                Message = "Success",
                ResponseData = responseData,
            });
        }


        [HttpGet]
        [Route("result-ok")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public ObjectResult ResulTokentOk(object responseData)
        {
            return StatusCode(StatusCodes.Status200OK, new CommonResponse
            {
                StatusCode = StatusCodes.Status200OK,
                IsValid = true,
                Message = "Token generated succsefully",
                ResponseData = responseData,
            });
        }

        [HttpGet]
        [Route("result-unauthorized")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public ObjectResult ResultUnauthorized()
        {
            return StatusCode(StatusCodes.Status401Unauthorized, new CommonResponse
            {
                StatusCode = StatusCodes.Status401Unauthorized,
                IsValid = false,
                Message = "Unauthorized",
                // ResponseData = null,
            });
        }

        [HttpGet]
        [Route("result-unauthorized")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public ObjectResult ResultInternalServer(string msg)
        {
            return StatusCode(StatusCodes.Status500InternalServerError, new CommonResponse
            {
                StatusCode = StatusCodes.Status500InternalServerError,
                IsValid = false,
                Message = "Internal Server Error",

            });
        }
    }
}
