﻿using AutoMapper;
using BrainerHubTaskWebApi.DomainLayer.Models;
using BrainerHubTaskWebApi.Helpers;
using BrainerHubTaskWebApi.ServiceLayer.Interface;
using BrainerHubTaskWebApi.Validations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BrainerHubTaskWebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : BaseController
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;

        public ProductsController(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }


        [HttpPost  ]
        [Route("GetAll-Products")]
        public async Task<IActionResult> GetProductListByFilter(ProductFilter filter)
        {
            try
            {
                var product = await _productService.GetProductList(filter);
                //var productList = _mapper.Map<List<ProductDto>>(product);

                if (product.TotalCount == 0)
                {
                    return ResultNotFound();
                }

                var data = new
                {
                    product.TotalCount,
                    product.TotalPages,
                    product.CurrentPage,
                    product
                };
                return ResultOk(data);
            }
            catch (Exception ex)
            {
                return ResultInternalServer(ex.Message);
                throw;
            }
        }

        [HttpGet]
        [Route("GetById-Products")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                var validationResult = new IdValidator().Validate(id);

                if (!validationResult.IsValid)
                {
                    return ResultBadRequestValidation(validationResult.Errors.FirstOrDefault()?.ErrorMessage);
                }

                var product = await _productService.GetById(id);
                if (product == null)
                {
                    return ResultNotFound();
                }

                var productDto = _mapper.Map<ProductDto>(product);

                return ResultOk(productDto);

            }
            catch (Exception ex)
            {
                return ResultInternalServer(ex.Message);
                throw;
            }
        }

        [HttpPost]
        [Route("Add-Products")]
        public async Task<IActionResult> AddProducts(AddProductDto addProductDto)
        {
            try
            {
                bool productExists = await _productService.ProductIsExists(addProductDto.ProductName);

                if (productExists)
                {
                    return ResultExixstBadRequest("Product Already Exists");
                }
                else
                {
                    var product = _mapper.Map<Products>(addProductDto);
                    var productDto = _productService.AddProduct(product);

                    addProductDto.ProductId = product.ProductId;

                    return ResultOk(addProductDto);
                }

            }
            catch (Exception ex)
            {
                return ResultInternalServer(ex.Message);
                throw;
            }
        }

        [HttpPut]
        [Route("Update-Products")]
        public async Task<IActionResult> UpdateProducts(UpdateProductDto updateProductDto)
        {
            try
            {
                var existingProduct = await _productService.GetById(updateProductDto.ProductId);

                if (existingProduct == null)
                {
                    return ResultNotFound();
                }

                bool productExists = await _productService.ProductIsExists(updateProductDto.ProductName);

                if (!productExists || existingProduct.ProductName == updateProductDto.ProductName)
                {
                    var product = _mapper.Map<Products>(updateProductDto);
                    var productDto = _productService.UpdateProduct(product);

                    return ResultOk(updateProductDto);
                }
               
                else
                {
                    return ResultExixstBadRequest("Product Already Exists");
                }
            }
            catch (Exception ex)
            {
                return ResultInternalServer(ex.Message);
                throw;
            }
            return ResultInternalServer("An unexpected error occurred.");
        }


        [HttpDelete]
        [Route("Delete-Products")]
        public async Task<IActionResult> DeleteProducts(int id)
        {
            try
            {
                var validationResult = new IdValidator().Validate(id);

                if (!validationResult.IsValid)
                {
                    return ResultBadRequestValidation(validationResult.Errors.FirstOrDefault()?.ErrorMessage);
                }
                var existingProduct = await _productService.GetById(id);

                if (existingProduct == null)
                {
                    return ResultNotFound();
                }
                await _productService.RemoveProduct(id);

                return ResultOk(null);

            }
            catch (Exception ex)
            {
                return ResultInternalServer(ex.Message);
                throw;
            }
        }

    }
}
