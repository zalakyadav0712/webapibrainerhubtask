﻿using BrainerHubTaskWebApi.DomainLayer.Context;
using BrainerHubTaskWebApi.DomainLayer.Models.Dto.UserDto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace BrainerHubTaskWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {
        
        private readonly IConfiguration _config;
        private readonly ProductContext _productContext;

        public AccountController(IConfiguration config, ProductContext productContext)
        {
            _config = config;
            _productContext = productContext;
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult Login([FromBody] UserLoginDto login)
        {
            IActionResult response = Unauthorized();
            var user = AuthenticateUser(login);

            if (user != null)
            {
                var tokenString = GenerateJSONWebToken(user);
                response = ResulTokentOk(new { token = tokenString });
            }

            return response;
        }

        private string GenerateJSONWebToken(UserLoginDto userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(ClaimTypes.Email, userInfo.Email),
            };

            var token = new JwtSecurityToken(
                _config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private UserLoginDto AuthenticateUser(UserLoginDto login)
        {
            var user = _productContext.Users.SingleOrDefault(u => u.Email == login.Email);

            //if (login.Email == "zlk@gmail.com" && login.Password == "z@l@k1212")
            //{
            //    return new UserLoginDto { Email = "zlk@gmail.com", Password = "z@l@k1212" };
            //}

            if (login.Email == user.Email && login.Password == user.Password)
            {
                return new UserLoginDto { Email = user.Email, Password = user.Password };
            }

            return null; // Authentication failed
        }
    }
}

