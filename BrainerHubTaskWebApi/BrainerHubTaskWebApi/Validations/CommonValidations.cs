﻿using BrainerHubTaskWebApi.DomainLayer.Models;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace BrainerHubTaskWebApi.Validations
{
    public class CommonValidations : Controller
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState
                    .Where(a => a.Value.Errors.Count > 0)
                    .SelectMany(x => x.Value.Errors)
                    .Select(error => error.ErrorMessage)
                    .ToList();

                var response = new CommonResponse
                {
                    StatusCode = StatusCodes.Status400BadRequest,
                    IsValid = false,
                    Message = string.Join(", ", errors),
                    ResponseData = null,
                };

                context.Result = new BadRequestObjectResult(response);
            }

            base.OnActionExecuting(context);
        }
    }

}

