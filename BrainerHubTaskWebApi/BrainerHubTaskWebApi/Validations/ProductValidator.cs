﻿using BrainerHubTaskWebApi.DomainLayer.Models;
using FluentValidation;

namespace BrainerHubTaskWebApi.Validations
{
    public class ProductValidator : AbstractValidator<ProductDto>
    {
        public ProductValidator()
        {
            RuleFor(x => x.ProductName).NotNull().NotEmpty().WithMessage("ProductName is required");
            //RuleFor(x => x.Description).NotNull().NotEmpty().WithMessage("Description is required");
            RuleFor(x => x.Price).GreaterThan(0).WithMessage("Price is required");
            RuleFor(x => x.Quentity).GreaterThan(0).WithMessage("Quentity is required");
        }
    }
}
