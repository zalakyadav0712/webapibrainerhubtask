﻿using BrainerHubTaskWebApi.DomainLayer.Models;
using FluentValidation;

namespace BrainerHubTaskWebApi.Validations
{
    public class UpdateProductValidator : AbstractValidator<UpdateProductDto>
    {
        public UpdateProductValidator()
        {
            RuleFor(x => x.ProductId).GreaterThan(0).WithMessage("ProductId is required");
            Include(new ProductValidator());
        }
    }
}
