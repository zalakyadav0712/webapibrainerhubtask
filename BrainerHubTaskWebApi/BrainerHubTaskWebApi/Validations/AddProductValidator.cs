﻿using BrainerHubTaskWebApi.DomainLayer.Models;
using FluentValidation;

namespace BrainerHubTaskWebApi.Validations
{
    public class AddProductValidator : AbstractValidator<AddProductDto>
    {
        public AddProductValidator()
        {
            Include(new ProductValidator());
        }
    }
}
