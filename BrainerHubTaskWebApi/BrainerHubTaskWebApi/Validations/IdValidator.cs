﻿using FluentValidation;

namespace BrainerHubTaskWebApi.Validations
{
    public class IdValidator : AbstractValidator<int>
    {
        public IdValidator()
        {
            RuleFor(id => id).GreaterThan(0).WithMessage("ProductId is required");
        }
    }
}
