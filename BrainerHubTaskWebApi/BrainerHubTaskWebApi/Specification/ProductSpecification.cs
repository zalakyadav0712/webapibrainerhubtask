﻿using BrainerHubTaskWebApi.DomainLayer.Models;
using System.Linq.Expressions;

namespace BrainerHubTaskWebApi.Specification
{
    //public class ProductNameSpecification : BaseSpecification<Products>
    //{

    //    private readonly string _productName;

    //    public ProductNameSpecification(string productName)
    //    {
    //        _productName = productName;
    //    }

    //    public Expression<Func<Products, bool>> Criteria
    //    {
    //        get
    //        {
    //            if (string.IsNullOrEmpty(_productName))
    //            {
    //                // If _productName is null or empty, return a predicate that always evaluates to true
    //                return product => true;
    //            }
    //            else
    //            {
    //                // If _productName has a value, filter by ProductName
    //                return product => product.ProductName == _productName;
    //            }
    //        }
    //    }

    //}

    public class ProductNameSpecification : ISpecification<Products>
    {
        private readonly string _productName;

        public ProductNameSpecification(string productName)
        {
            _productName = productName;
        }

        public Expression<Func<Products, bool>> Criteria
        {
            get
            {
                if (string.IsNullOrEmpty(_productName))
                {
                    return product => true; // Always true, no filter applied
                }

                return product => product.ProductName == _productName;
            }
        }
    }





    public class NotDeletedSpecification : ISpecification<Products>
    {
        public Expression<Func<Products, bool>> Criteria => product => (bool)!product.IsDeleted;
    }

    //public class QuantitySpecification : ISpecification<Products>
    //{
    //    private readonly int _quantity;

    //    public QuantitySpecification(int quantity)
    //    {
    //        _quantity = quantity;
    //    }

    //    public Expression<Func<Products, bool>> Criteria
    //    {
    //        get
    //        {
    //            if (_quantity == 0)
    //            {
    //                return product => true;
    //            }
    //            else
    //            {
    //                return product => product.Quentity == _quantity;
    //            }
    //        }
    //    }
    //}
    ////public class NotDeleted1Specification : ISpecification<Products>
    //{
    //    public Expression<Func<Products, bool>> Criteria => product => !product.IsDeleted;

    //    public bool IsSatisfiedBy(Products entity)
    //    {
    //        // Implement the additional checks directly here if needed.
    //        // For this specific specification, you can return the result of the Criteria.
    //        return !entity.IsDeleted;
    //    }
    //}

}
