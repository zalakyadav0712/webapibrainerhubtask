﻿using BrainerHubTaskWebApi.DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace BrainerHubTaskWebApi.RepositoryLayer.Interface
{
    public interface IProductRepository
    {
        Task<List<Products>> GetProducts();

        Task<Products> GetById(int id);

        Task AddProduct(Products products);


        Task<bool> RemoveProduct(int id);

        Task UpdateProduct(Products products);

        Task<IEnumerable<Products>> GetAllProducts(Expression<Func<Products, bool>> predicate);

        Task<bool> ProductIsExists(string productName);

    }

}
