﻿using BrainerHubTaskWebApi.DomainLayer.Context;
using BrainerHubTaskWebApi.DomainLayer.Models;
using BrainerHubTaskWebApi.RepositoryLayer.Interface;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace BrainerHubTaskWebApi.RepositoryLayer.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly ProductContext _productContext;

        public ProductRepository(ProductContext productContext)
        {
            _productContext = productContext;
        }
        public async Task AddProduct(Products products)
        {
            try
            {
                 _productContext.AddAsync(products);
                 _productContext.SaveChangesAsync();

                await GetById(products.ProductId);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public async Task<Products> GetById(int id)
        {
            try
            {
                return await _productContext.Products.FirstOrDefaultAsync(x => x.ProductId == id && x.IsDeleted == false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public async Task<IEnumerable<Products>> GetAllProducts(Expression<Func<Products, bool>> predicate)
        {
            return await _productContext.Products
                .Where(predicate)
                .ToListAsync();
        }
        public async Task<List<Products>> GetProducts()
        {
            try
            {
               return await _productContext.Products.ToListAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
        
        public async Task UpdateProduct(Products products)
        {
            try
            {
                 _productContext.Products.Update(products);
                  _productContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public async Task<bool> RemoveProduct(int id)
        {
            try
            {
                var product = _productContext.Products.FirstOrDefault(x => x.ProductId == id && x.IsDeleted == false);

                if (product != null)
                {
                    product.IsDeleted = true;
                    _productContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public async Task<bool> ProductIsExists(string productName)
        {
            bool productExists = await _productContext.Products.AnyAsync(x => x.ProductName == productName && x.IsDeleted == false);

            if (productExists)
            {
                return true;
            }
            return productExists;
        }
    }
}
