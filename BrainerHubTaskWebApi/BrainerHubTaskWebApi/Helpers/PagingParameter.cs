﻿using System.ComponentModel;

namespace BrainerHubTaskWebApi.Helpers
{
    public class PagingParameter
    {

        [DefaultValue(1)]
        public int PageNumber { get; set; } = 1;
       
        private int _pageSize = 10;

        [DefaultValue(10)]
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }
    }
}
