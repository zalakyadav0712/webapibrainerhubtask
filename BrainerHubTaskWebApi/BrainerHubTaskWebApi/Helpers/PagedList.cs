﻿namespace BrainerHubTaskWebApi.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PagedList<T>
    {
        public List<T> Items { get; private set; }
        public int PageNumber { get; private set; }

        public int CurrentPage { get; private set; }


        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }
        public int TotalPages => (int)Math.Ceiling(TotalCount / (double)PageSize);

        public bool HasPreviousPage => PageNumber > 1;
        public bool HasNextPage => PageNumber < TotalPages;

        public PagedList(List<T> items, int pageNumber, int pageSize, int totalCount)
        {
            Items = items;
            PageNumber = pageNumber;
            PageSize = pageSize;
            TotalCount = totalCount;
            CurrentPage = PageNumber;
        }

        public static PagedList<T> ToPagedList(IEnumerable<T> source, int pageNumber, int pageSize)
        {
            var totalCount = source.Count();
            var items = source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return new PagedList<T>(items, pageNumber, pageSize, totalCount);
        }
    }

}

