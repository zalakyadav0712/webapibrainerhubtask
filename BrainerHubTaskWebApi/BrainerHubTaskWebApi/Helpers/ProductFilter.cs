﻿using System.ComponentModel;

namespace BrainerHubTaskWebApi.Helpers
{
    public class ProductFilter : PagingParameter
    {
        [DefaultValue("")]
        public string? ProductName { get; set; } = string.Empty;

        //public int? Quentity { get; set; } = 0;

    }
}
