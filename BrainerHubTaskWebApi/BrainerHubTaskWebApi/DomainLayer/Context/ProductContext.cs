﻿using BrainerHubTaskWebApi.Configuration;
using BrainerHubTaskWebApi.DomainLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace BrainerHubTaskWebApi.DomainLayer.Context
{
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options)
             : base(options)
        {
        }

        protected ProductContext()
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }

        public DbSet<Products> Products { get; set; }

        public DbSet<Users> Users { get; set; }
    }
}
