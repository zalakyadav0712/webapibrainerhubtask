﻿namespace BrainerHubTaskWebApi.DomainLayer.Models
{
    public class ProductDto
    {
        public int ProductId { get; set; }

        public string? ProductName { get; set; } = string.Empty;

        public string? Description { get; set; } = string.Empty;

        public double? Price { get; set; } = 0;

        public int? Quentity { get; set; } = 0;
    }
}
