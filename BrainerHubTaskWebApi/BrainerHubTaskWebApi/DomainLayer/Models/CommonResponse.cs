﻿namespace BrainerHubTaskWebApi.DomainLayer.Models
{
    public class CommonResponse
    {
        public int StatusCode { get; set; }

        public bool IsValid { get; set; }

        public string Message { get; set; }

        public object ResponseData { get; set; }
    }
}
