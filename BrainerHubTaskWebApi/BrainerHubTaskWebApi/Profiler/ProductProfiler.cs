﻿using AutoMapper;
using BrainerHubTaskWebApi.DomainLayer.Models;

namespace BrainerHubTaskWebApi.Profiler
{
    public class ProductProfiler : Profile
    {
        public ProductProfiler()
        {
            CreateMap<Products, ProductDto>().ReverseMap();
            CreateMap<Products, ProductDto>();
            CreateMap<ProductDto, Products>().ReverseMap();
            CreateMap<Products, AddProductDto>().ReverseMap();
            CreateMap<Products, UpdateProductDto>().ReverseMap();
        }
    }
}
